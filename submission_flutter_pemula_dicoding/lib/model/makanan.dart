class Makanan {
  String nama;
  String imgAsset;
  List<dynamic> listResep;

  Makanan(
      {required this.nama, required this.imgAsset, required this.listResep});
}

class Masakan {
  String name;
  String imageUrl;
  String bahan;
  String proses;

  Masakan(
      {required this.name,
      required this.imageUrl,
      required this.bahan,
      required this.proses});
}

//Data
var makananList = [
  Makanan(nama: 'Desert', imgAsset: 'assets/img/Desert.png', listResep: [
    Masakan(
        name: 'Pakcage Mangga',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2019/09/24/171169/1101683-1000xauto-resep-dessert-enak.jpg',
        bahan:
            'ulit Pancake:\n- 125 gr tepung terigu\n- 1 butir telur ayam, dikocok lepas\n- 1/4 sdt garam\n- 275 ml susu cair\n-\n-Isian:\n- Whipping cream\n- Mangga',
        proses:
            'Campurkan tepung terigu, garam dan telur jadi satu\n- Tambahkan susu cair secara perlahan sambil diaduk-aduk. Kemudian disaring\n- Dadar di tempat penggoreng sampai adonan habis\n- Isi kulit pancake dengan whipping cream dan mangga, lipat seperti amplop\n- Simpan pancake di kulkas. Pancake mangga enak dimakan dalam kondisi dingin'),
    Masakan(
        name: 'Pudding Custard',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2019/09/24/171169/1101685-1000xauto-resep-dessert-enak.jpg',
        bahan:
            '- 1 bungkus agar-agar (7 gr)\n- 800 cc air\n- 1 liter susu cair\n- 8 sdm tepung custard\n- 20 sdm gula pasir (sesuai selera)\n- 4 butir kuning telur, kocok lepas\n- 1/2 sdt garam\n- 1 sdt essence vanila\n- 2 sdt rum (boleh tidak memakainya)',
        proses:
            'Campur semua bahan ke dalam panci kecuali rum, aduk rata\n- Panaskan di atas kompor dengan api sedang sambil terus diaduk hingga mendidih dan mengental. Matikan api dan tambahkan rum, aduk terus sampai agak dingin\n- Tuang ke dalam cetakan dan biarkan beku. Hias atasnya dengan buah\n- Siap dihidangkan.'),
    Masakan(
        name: 'Es CIncau Pandan',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2019/09/24/171169/1101688-1000xauto-resep-dessert-enak.jpg',
        bahan:
            '1 Bungkus cincau (15gr)\n- 800 ml air\n- Pasta pandan secukupnya\n- 50 gr gula pasir',
        proses:
            'Campur semua bahan jadi satu, gula pasir, air, pasta pandan, aduk rata. Masak dengan api sedang sambil sesekali diaduk sampai mendidih\n- Matikan api, diamkan sampai uap panasnya hilang. Simpan di kulkas\n- Setelah cincau jadi potong-potong sesuai selera, siap digunakan untuk campuran es dan minuman\n- Sajikan.'),
    Masakan(
        name: 'Pudding Roti Tawar',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2019/09/24/171169/1101692-1000xauto-resep-dessert-enak.jpg',
        bahan:
            '- 8 lembar roti tawar, sobek/potong kotak\n- 4 butir telur\n- 50 gram gula pasir\n- 50 gram gula palem\n- 2 sdm munjung butter\n- 400 ml susu cair full cream\n- 50 gram keju cheddar parut\n- 1/2 sdt garam\n- 1/2 sdt vanila\n- 1/4 sdt kayu manis bubuk\n-\n-Topping:\n- Secukupnya almonds slice, kismis, kayu manis bubuk, gula palem, pisang, chocochip, keju dan lain-lain sesuai selera',
        proses:
            '- Panaskan susu cair dan butter, cukup sampai butter leleh saja, masukkan keju parut, aduk rata, matikan api, sisihkan\n- Kocok telur, gula, garam dan vanilla sampai gula larut, tuangi susu hangat, beri kayumanis bubuk, aduk rata\n- Olesi ramekin dengan margarin, tata roti tawar didasar ramekin, taburi chocochip, kismis dan almond slice, tuangi adonan telur, biarkan sampai cairan terserap roti, tata kembali roti tawar diatas adonan, tuangi adonan telur perlahan, biarkan sampai terserap, taburi topping sesuai selera\n- Masukkan oven yang sudah dipanaskan terlebih dahulu, panggang dengan suhu 160 derajat Celcius api atas bawah sampai matang (kurang lebih 40-45 menit), keluarkan dari oven, taburi kayu manis bubuk, bisa disajikan hangat ataupun dingin'),
    Masakan(
        name: 'Pudding Chocolate',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2019/09/24/171169/1101696-resep-dessert-enak.jpg',
        bahan: '- Premix Puding Chocolate\n- Air\n- Whipcream\n- Chocochips',
        proses:
            '- Campurkan 1 kg Premix Puding Chocolate dengan 3000 cc air (15 gelas)\n- Panaskan dengan api sedang hingga mendidih. Biarkan mendidih selama beberapa detik sebelum diangkat\n- Angkat dan tuang dalam gelas/cetakan\n- Tambahkan whipcream dan chocochips sesuai selera.'),
    Masakan(
        name: 'Desert Mutiara Mangga',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2019/09/24/171169/1101701-1000xauto-resep-dessert-enak.jpg',
        bahan:
            '- 2 buah mangga, potong-potong\n- 10 buah biskuit marie, hancurkan/blender\n- 1 bungkus sagu mutiara\n- 1 bungkus santan instan- 1 bungkus susu kental manis putih\n- 100 ml air\n- 1 sdm maizena larutkan dengan sedikit air\n- Sejumput garam\n- Gula secukupnya (optional, sesuaikan selera)',
        proses:
            '- Rebus mutiara sampai mengembang dan matang (bisa ditambah gula bila suka manis), sisihkan\n- Campur santan, susu kental manis, air, gula, dan garam. Rebus sampai mendidih\n- Masukkan larutan maizena. Aduk rata, masak sampai mendidih. Tes rasa\n- Angkat dan dinginkan\n- Tata biskuit yang sdh dihancurkan, beri mutiara, beri potongan mangga, beri vla santan, begitu seterusnya sampai gelas penuh\n- Sajikan dingin lebih enak.'),
    Masakan(
        name: 'Banana milk crispy ',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2019/09/24/171169/1101699-1000xauto-resep-dessert-enak.jpg',
        bahan:
            '- 2 lembar puff pastry instant\n- 2 buah pisang\n- Keju dan meises secukupnya\n- 100 ml susu cair\n- 1 butir kuning telur\n- 1/4 sdt banana essence\n- 1/2 sdm tepung maizena\n- 30 gr gula halus\n- Pewarna kuning',
        proses:
            '- Dalam wadah, campurkan susu dengan telur, tepung maizena, gula halus, banana essence dan pewarna kuning, aduk hingga rata, sisihkan\n- Siapkan loyang berbentuk pisang, letakkan puff pastry diatasnya lalu potong puff pastry mengikuti bentuk loyang\n- Letakkan meises di atas adonan lalu tambahkan pisang yang sudah dipotong kecil-kecil\n- Tuang adonan susu hingga menutupi pisang, lalu taburi keju dan meises diatasnya\n- Panggang di dalam oven dengan suhu 180 derajat Celcius api atas bawah selama, kurang lebih 30 menit hingga kecoklatan, angkat, lepaskan dari loyang.\n- Sajikan.')
  ]),
  Makanan(
      nama: 'Middle East',
      imgAsset: 'assets/img/Middle_East.png',
      listResep: [
        Masakan(
            name: 'Nasi Kebuli',
            imageUrl:
                'https://img-global.cpcdn.com/recipes/14a5528d54e5bcfd/1360x964cq70/nasi-kebuli-sederhana-foto-resep-utama.webp',
            bahan:
                '- 4 cup beras biasa, cuci bersih\n- 500 gr daging sapi atau kambing atau ayam\n- 1 bungkus santan instan 65 ml\n- 750 ml air\n- 3 sdm mentega\n- 3 biji bunga lawang\n- 3 biji kapulaga\n- 2 biji kayu manis\n- 10 biji cengkeh\n- 2 sdm saus tomat\n- 1 sdt garam\n- 1/2 sdt gula\n- 1/2 sdt kaldu jamur\n- 1 cm Jahe memarkan\n-\n- Bumbu halus\n- 4 siung bawang merah\n- 4 siung bawang putih\n- 1 sdt jinten\n- 1/4 sdt pala\n- 1 bungkus bumbu kari bubuk merk desaku',
            proses:
                'Rebus daging hingga empuk, potong2 kecil, lalu lumuri dengan saus tomat 20 menit\n-Tumis margarin hingga harum, tambahkan bumbu halus dan bubuk kari, masukkan bunga lawang, cengkeh, kayu manis dan kapulaga, masukkan daging, lalu beri santan dan air, masak hingga mendidih tambahkan gula, garam dan kaldu jamur,\n-Jika sudah ambil sebagian dagingnya dan goreng,\n-Siapkan beras, beri takaran air daging sesuai takaran, dan masak hingga air menyusut sesekali aduk hingga tercampur rata, jika sudah menyusut, lalu kukus dengan sebagian daging yang digoreng tadi, hingga matang\n-Dan sajikan dg taburan bawang merah goreng'),
        Masakan(
            name: 'Luqaimat',
            imageUrl:
                'https://ds393qgzrxwzn.cloudfront.net/resize/m720x480/cat1/img/images/0/PlyopP2m11.jpg',
            bahan:
                '3 sendok makan tepung maizena\n-2 cangkir susu\n-2,5 cangkir tepung terigu serba guna\n-1 sendok makan ragi instan\n-½ sendok makan gula pasir\n-3 sendok makan yoghurt\n-Garam secukupnya\n-Sirop kurma secukupnya (bisa diganti dengan sirop maple)\n-Minyak sayur secukupnya',
            proses:
                'Silahkan ambil wadah lalu campurkan susu, tepung terigu, ragi instan, gula pasir, tepung maizena, garam dan yogurt.\n-Aduk rata dan diamkan hingga 4 jam sampai mengembang.\n-Aduk kembali sampai gelembung udara menghilang lalu bentuk adonan pakai sendok.\n-Bentuk bola lalu goreng di minyak panas.\n-Angkat jika sudah kecoklatan lalu sajikan dengan tambahan sirup kurma'),
        Masakan(
            name: 'Hummus',
            imageUrl:
                'https://ds393qgzrxwzn.cloudfront.net/resize/m720x480/cat1/img/images/0/v5uBJSgKVp.jpg',
            bahan:
                '100 gram kacang hummus/kacang arab\n-3 sdm tahina/tahini\n-1 buah lemon jus\n-2 siung Bawang putih\n-1/2 sdt jinten bubuk\n-secukupnya garam\n-6 sdm Minyak zaitun / olive oil\n-secukupnya air\n-\n-',
            proses:
                'Rebus kacang usai direndam semalaman jika memakai kacang kering. Kalau pakai yang kalengan bisa langsung digunakan\n-Masukkan kacang dalam blender lalu campur dengan lemon jus, jintan bubuk dan garam.\n-Tambahkan tahini dan minyak zaitun lalu masukkan air.\n-Blender sampai berbentuk pasta lalu sajikan dengan disiram olive oil.'),
        Masakan(
            name: 'Roti Khubz',
            imageUrl:
                'https://cdn.idntimes.com/content-images/community/2019/03/pita-6d53c8a4d19c49c56005736bc54b98aa.jpg',
            bahan:
                '250 gr tepung terigu\n-2 sdm susu bubuk putih\n-1 sdt gula pasir\n-5 gr ragi instan\n-1 sdm minyak\n-1 sdt garam\n-Air secukupnya',
            proses:
                'Masukkan secangkir air bersama dengan gula dan ragi instan ke dalam sebuah baskom . Pastikan ragi aktif dengan ditandai adanya gelembung-gelembung kecil.\n-Masukkan tepung terigu, susu bubuk dan minyak. Uleni hingga kalis.\n-Diamkan adonan hingga mengembang selama 30 menit.\n-Setelah mengembang, taburkan garam dan uleni ulang.\n-Taburkan tepung di alat penggiling agar adonan tidak lengket, giling dengan membuat bulatan pipih. Kamu dapat mengukur ketebalan sesuka hati.\n-Nyalakan kompor dengan api lilin. Siapkan teflon tanpa diberi apa-apa.\n-Ketika teflon telah panas letakkan adonan. Bolak balik sampai kematangan yang diinginkan'),
        Masakan(
            name: 'Kofta',
            imageUrl:
                'https://cdn.idntimes.com/content-images/community/2019/03/mini-lamb-kofta-77705-1-8d1eee464426209448a0cf7f49896a71.jpeg',
            bahan:
                '250 gr daging sapi giling\n-3 sdm tepung roti\n-1 butir telur\n-1 siung bawang bombay\n-2 lembar daun bawang\n-1/2 sdt pala bubuk\n-1/2 sdt lada hitam bubuk\n-Sejumput kayu manis bubuk\n-1 sdt garam\n-1/2 sdt basil kering\n-1 sdt cabai bubuk\n-Potongan keju mozarella sesuai selera',
            proses:
                'Potong bawang bombay kecil-kecil dengan bentuk dadu.Tumis hingga harum dan layu.\n-Iris tipis daun bawang.\n-Campur daging sapi giling,daun bawang, telur, bawang bombay dan tepung roti di sebuah wadah.\n-Taburkan pala bubuk, lada hitam bubuk, kayu manis bubuk, cabai bubuk dan garam. Aduk hingga merata.\n-Buat adonan daging giling dengan bentuk bulat. Jangan lupa letakkan potongan keju mozarella di tengahnya.\n-Goreng kofta dengan api kecil. Angkat ketika berwana kecoklatan'),
        Masakan(
            name: 'Menemen Turkish Food',
            imageUrl:
                'https://img-global.cpcdn.com/recipes/14fffa2aa3337b4d/1360x964cq70/menemen-turkish-food-foto-resep-utama.webp',
            bahan:
                '3 btr telur\n-1 bh bombay\n-1 bh sosis\n-1 bh tomat\n-7 bh cabe rawit\n-2 sdm saos tomat\n-1 sdm saus cabe\n-2 sdm bubuk cabe\n-1 sdm saus tiram\n-2 sdm kecap asin\n-1 sdm minyak wijen\n-Secukupnya keju',
            proses:
                'Cincang bombay, cabe, sosis dan tomat, panaskan teflon, beri sedikit minyak, tumis bombay sampai layu masukkan tomat, sosis dan cabe aduk merata sampai layu\n-Masukkan semua saus, tambahkan sedikit air, tambahkan minyak wijen dan kecap asin, tes rasa\n-Masukkan telur dan aduk pelan asal rata, jika sdh setengah matang tambahkan keju parut dan angkat, sajikan dg roti atau nasi.'),
        Masakan(
            name: 'Nasi kabsa ayam',
            imageUrl:
                'https://img-global.cpcdn.com/recipes/cfd2b62ba3e65506/751x532cq70/nasi-kabsah-foto-resep-utama.jpg',
            bahan:
                '1/2 ekor Ayam\n-1 Bawang Bombay besar (potong dadu)\n-3 Bawang putih (cincang halus)\n-1 Tomat (porong dadu)\n-2 sendok Tomat paste:\n-secukupnya kayumanis\n-secukupnya daun salam\n-secukupnya kapolaga\n-secukupnya cengkeh\n-Bumbu kabsa (banyak di jual)\n-1 cup Beras basmati (cuci bersih)\n-1 blok Kaldu blok\n-secukupnya Garem',
            proses:
                'Tumis bawang bombay + rempah, sampai layu dan harum.\n-Masukin tomat + tomat paste + kaldu blok + garem + bumbu kabsa ,aduk.\n-Masukin ayam + air (masak sampai ayam mateng)\n-Setelah ayam mateng, matikan api, angkat ayam, masukan k oven panggang sampai coklat keemasan Sisa air, di saring, masukan beras\n-Masak sampai beras matang\n-(masak beras di rice cooker)\n-Penyajian: Taruh nasi kabsa di piring stainless steel (atau sesuai selera anda) lalu taruh ayam di atas nasi nya\n-Selamat mencoba'),
      ]),
  Makanan(
      nama: 'Tradisional',
      imgAsset: 'assets/img/Traditional.png',
      listResep: [
        Masakan(
            name: 'Gudeg',
            imageUrl:
                'https://cdn-brilio-net.akamaized.net/news/2021/04/08/203481/1443511-resep-masakan-jawa-tradisional.jpg',
            bahan:
                '- 1 kg nangka muda, potong kotak lalu rebus sebentar dengan sedikit garam, angkat, tiriskan\n- 400 gram gula merah, sisir\n- 4 batang serai, geprek\n- 4 ruas lengkuas, geprek\n- 5 lembar daun salam\n- 4 lembar daun jeruk\n- 1 liter santan cair\n- 700 ml air kelapa\n- 1 liter santan kental\n- Garam secukupnya\n- 3 lembar daun jati\n- Telur rebus\n- 100 gram bawang merah\n- 10 siung bawang putih\n- 8 butir kemiri sangrai\n- 20 gram ketumbar bubuk',
            proses:
                'Tata daun jati di dasar panci, lalu masukkan nangka muda dan semua bumbu. Tuang santan cair dan juga air kelapa. Kemudian tutup kembali dengan daun jati dan tutup panci, masak hingga kuah sedikit menyusut.\n-Setelah kuah sedikit menyusut tambahkan gula merah, garam, dan santan kental. Tutup kembali dengan daun jati dan tutup panci, lanjutkan memasak dengan api kecil.\n-Koreksi rasa. Terakhir tambahkan telur rebus, kemudian tutup panci kembali, lanjut masak gudegnya hingga matang dan benar-benar kering selama 8-10 jam.\n-Sajikan.'),
        Masakan(
            name: 'mie Lethek',
            imageUrl:
                'https://cdn-brilio-net.akamaized.net/news/2021/04/08/203481/1443515-1000xauto-resep-masakan-jawa-tradisional.jpg',
            bahan:
                '- 3 lembar mi lethek kering\n- 6 siung bawang putih\n- 2 siung bawang merah\n- 1 butir kemiri\n- Garam, merica, dan kecap secukupnya\n- 1 buah wortel, iris seperti korek\n- 1 butir telur ayam\n- 1/4 buah kubis\n- Daun bawang dan daun seledri secukupnya\n- Bawang goreng secukupnya untuk taburan\n-',
            proses:
                '1. Rendam mi lethek dengan air matang sampai lunak, tiriskan.\n-2. Haluskan bawang merah, bawang putih, dan kemiri.\n-3. Tumis bumbu halus sampai harum, masukkan telur dan wortel. Setelah wortel agak matang, masukkan irisan kubis, tambahkan kecap.\n-4. Masukkan mi, daun bawang, dan daun seledri. Aduk hingga rata. Tambahkan garam dan merica. Koreksi rasa.\n-5. Sajikan mi lethek goreng dengan taburan bawang goreng.'),
        Masakan(
            name: 'Getuk',
            imageUrl:
                'https://cdn-brilio-net.akamaized.net/news/2021/04/08/203481/1443512-resep-masakan-jawa-tradisional.jpg',
            bahan:
                ' kg singkong, kupas cuci lalu potong-potong\n- 150 gram kelapa parut\n- 100 gram gula pasir\n- Pewarna makanan\n- 1 sdt garam\n-Secukupnya kelapa parut + sejumput garam lalu kukus sebentar',
            proses:
                '1. Panaskan kukusan.\n-2. Kukus singkong bersama 150 gram kelapa parut hingga matang, kira-kira selama 30 menit.\n-3. Angkat dan pindahkan dalam wadah yang cukup besar, lalu taburi 1 sdt garam dan 100 gram gula pasir. Aduk rata.\n-4. Siapkan alat penggiling singkong. Giling sedikitsedikit hingga habis.\n-5. Bagi menjadi 3 bagian. Beri pewarna sesuai selera.\n-6. Kemudian giling lagi bentuk panjang, tata di piring dansiap disajikan bersama kelapa parut'),
        Masakan(
            name: 'Soto Lamongan',
            imageUrl:
                'http://blog.sayurbox.com/wp-content/uploads/2020/09/the.lucky_.belly_-960x666.jpg',
            bahan:
                '10 siung bawang merah\n-8 siung bawang putih\n-1/4 sdt pala bubuk\n-1 sdm ketumbar bubuk\n-5 butir kemiri sangrai\n-1/2 sdm kunyit bubuk\n-1 ruas jahe\n-Lengkuas (secukupnya)\n-2-3 batang sereh\n-5 lembar daun jeruk\n-Seledri\n-Daun pre',
            proses:
                'Cuci bersih ayam dan lumuri jeruk nipis supaya amisnya hilang.\n-Haluskan bahan-bahan bumbu halus, lalu gongsong sampai berwarna kecoklatan.\n-Setelah itu, masukkan bumbu cemplung. Tunggu sampai harum, lalu tambahkan sedikit air agar tidak gosong.\n-Kalau sudah harum, masukkan ayam, tambahkan air, masukkan seledri yang diikat, serta  potongan daun pre. Tambahkan juga gula, garam, merica bubuk, dan penyedap rasa.\n-Tunggu hingga mendidih sambil cek kematangan.\n-Jika sudah matang, tinggal tambahkan bumbu pelengkap, seperti jeruk nipis, daun pre, seledri, bawang goreng, dan telur rebus.\n-Jika sudah, tinggal angkat dan sajikan'),
        Masakan(
            name: 'Pecel Madiun',
            imageUrl:
                'https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1601776952/t1kycswgiyq3swwipt91.jpg',
            bahan:
                '250 gram kacang tanah\n-5 siung bawang putih\n-3 buah cabai merah besar\n-5 lembar daun jeruk\n-1/2 sdm asam jawa\n-1 ruas kencur\n-50 gram gula merah sisir\n-1 sdm garam\n-1 sdm kaldu bubuk\n-Bahan pelengkap (sayuran direbus):\n-Taoge\n-Kangkung\n-Kacang panjang\n-Tahu goreng\n-Tempe goreng',
            proses:
                'Goreng kacang tanah hingga matang. Sisihkan.\n-Goreng juga bahan-bahan bumbu, seperti bawang putih, kencur, daun jeruk, dan cabai merah besar. Dinginkan hingga suhu ruangan.\n-Setelah dingin, campurkan bahan-bahan bumbu tersebut dengan kacang tanah yang sudah digoreng. Tambahkan gula merah, asam jawa, garam, dan kaldu bubuk. Haluskan. Siap digunakan.\n-Ambil sedikit bumbu, beri air panas, kemudian aduk rata.\n-Siapkan sayuran rebus dalam piring saji. Siram dengan kuah kacang. Sajikan dengan tahu dan tempe goreng. Siap disantap.'),
        Masakan(
            name: 'Nasi Krawu',
            imageUrl:
                'https://cdn.idntimes.com/content-images/community/2021/03/fromandroid-e4e6ea1428a2310607c08ea4784e41c9_600x400.jpg',
            bahan:
                '1/2 kg daging sapi\n-3 buah gula merah\n-2 lembar daun salam\n-Asam jawa (secukupnya)\n-8 siung bawang merah\n-5 siung bawang putih\n-1 sdm ketumbar\n-2 buah cabai merah besar\n-5 buah cabai rawit\n-200 ml air kaldu rebusan daging\n-Minyak goreng',
            proses:
                'Cuci bersih daging sapi, kemudian presto kurang lebih selama 10 menit. \n-Setelah daging matang, suwir menggunakan garpu. \n-Blender semua bahan bumbu halus, masukkan daun salam, dan tumis hingga harum. \n-Kemudian, masukkan air asam jawa dan gula merah, gula pasir, garam, merica bubuk, serta penyedap rasa. \n-Masukkan daging suwir, diamkan sampai mengering, tes rasa, dan siap disajikan.'),
      ]),
  Makanan(nama: 'Western', imgAsset: 'assets/img/Western.png', listResep: [
    Masakan(
        name: 'Lemon Herb Roasted Potatoes',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2019/08/19/169174/1083246-1000xauto--resep-western-food.jpg',
        bahan:
            '- 2 Buah kentang ukuran sesuai selera\n- 2 Sdm salted butter, lelehkan\n- 3 Siung bawang putih, cincang\n- 2 Sdm air perasan lemon\n-0 3 Sdm madu\n- Sejumput garam\n- 1 Sdm parsley\n- 1/2 Sdt lada hitam',
        proses:
            '- Panaskan oven suhu 175 derajat celcius\n- Siapkan loyang atau wadah tahan panas, lapisi dengan aluminium foil\n- Cuci bersih kentang, jangan dikupas kulitnya\n- Potong tipis kentang, namun jangan sampai patah\n- Campur semua bahan aduk jadi satu sampai rata\n- Oleskan ke kentang, pastikan tiap sela potongan terkena olesan bumbu\n- Panggang suhu 175 derajat celcius selama 30 menit\n- Oles lagi bumbu, kemudian panggang lagi selama 30 menit\n- Sajikan'),
    Masakan(
        name: 'French Toast',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2019/08/19/169174/1083247-1000xauto--resep-western-food.jpg',
        bahan:
            'Roti tawar\n- Telur 1bh\n- Vanili\n- Gula pasir 1 sdm\n- Susu cair plain',
        proses:
            '- Kocok telur, vanili, gula pasir dan susu cair\n- Balurkan roti dalam adonan\n- Panggang dalam margarin cair\n- Masak hingga kecokelatan\n- Sajikan dengan es krim'),
    Masakan(
        name: 'Rosemary Chicken with Sauteed Vegetables',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2019/08/19/169174/1083253-1000xauto--resep-western-food.jpg',
        bahan:
            '- Fillet ayam paha/dada\n- Jagung pipil\n- 1 Batang wortel potong korek api\n- Garam\n- Merica\n- Lada butir\n- Rosemary\n- Thyme',
        proses:
            '- Bumbui ayam dengan garam, lada butir, rosemary dan thyme\n- Panaskan panggangan/teflon dengan butter lalu grill ayam hingga matang\n- Panaskan wajan dengan sedikit butter lalu tumis jagung dan wortel, tambahkan sedikit air dan bumbui lalu masak hingga matang\n- Sajikan'),
    Masakan(
        name: 'Tori No Teba',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2019/08/19/169174/1083257-1000xauto--resep-western-food.jpg',
        bahan:
            '- 6 Potong sayap ayam\n- 100 gr minced chicken\n- 2 sdm tepung panir\n- 1 Butir telur\n- 1 Batang daun bawang cincang\n- 50 ml air\n- 1 Siung bawang putih, cincang halus\n- Kecap asin\n- Garam\n- Merica\n- Kaldu bubuk\n- Minyak wijen\n- Gula',
        proses:
            '- Patahkan persendian ayam hingga dapat diluruskan\n- Potong bagian sayap, seperti paha, lalu pisahkan tulang dan dagingnya\n- Ambil sayap yang sudah diluruskan, perlahan-lahan keluarkan daging yang berada di dalam tulang, jangan sampai kulit terpotong\n- Campurkan semua bahan dan bumbu lalu masukkan ke dalam sayap ayam\n- Kukus hingga matang selama 45 menit\n- Goreng dalam minyak panas hingga kecokelatan'),
    Masakan(
        name: 'Spicy tuna roll',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2019/08/19/169174/1083255-1000xauto--resep-western-food.jpg',
        bahan:
            '- Siapkan 5 ons ikan tuna cincang\n- 1 Sendok makan saus sriracha (saus pedas Thailand)\n- 1/2 Sendok teh minyak cabai\n- 1/2 Sendok teh rice vinegar\n- 2 Sendok makan bawang daun cincang\n- 1,5 Sendok makan mayones rendah lemak\n- 6 Lembar nori\n- 1,5 Cup nasi Jepang',
        proses:
            '- Campur daging tuna, saus sriracha, minyak cabai, rice vinegar, bawang daun cincang, dan mayones\n- Siapkan tatami atau sushi roller, letakkan lembaran nori di atas tatami. Tambahkan nasi di atas rumput laut dan sebarkan hingga 1/4 bagian nori. Usahakan jangan terlalu tebal\n- Letakkan tuna di tengah nasi\n- Gulung perlahan, dan beri sedikit air di ujung lembaran nori untuk merekatkan\n- Potong gulungan sushi menjadi beberapa bagian\n- Sajikan di piring bersama kecap asin atau wasabi'),
    Masakan(
        name: 'Chickpea Salad with Lemon, and Tuna',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2019/08/19/169174/1083254--resep-western-food.jpg',
        bahan:
            '- Siapkan 15 ons kacang chickpea\n- 1 Kaleng tuna\n- 1/2 Cup buah zaitun Kalamata, cincang kasar\n- 1/3 Cup bawang bombay cincang kasar\n- 1/4 Cilantro, cincang kasar\n- 1/2 Sendok teh garam\n- 1 Sendok makan perasan lemon\n- 2 Sendok makan minyak zaitun\n- Lada hitam secukupnya',
        proses:
            '- Campur semua bahan di satu mangkuk besar\n- Sajikan dengan roti Prancis panggang'),
    Masakan(
        name: 'Sun Dried Tomato Basil Pasta',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2019/08/19/169174/1083258-1000xauto--resep-western-food.jpg',
        bahan:
            '- 1 Kotak pasta atau spaghetti (varian bisa disesuaikan)\n- 1/4 Tomat sun dried cincang (Tomat Italia yang sudah dikeringkan dengan matahari dan direndam dalam minyak)\n- 1/2 Sendok makan bawang putih cincang\n- 1 Sendok makan cuka balsamic\n- 1/2 Cup daun basil, cincang kasar\n- 1/2 Buah bawang bombay ukuran medium, potong tipis\n- 1/3 Cup minyak zaitun\n- 3 Sendok makan susu almond tawar\n- 1/4 Sendok teh garam\n- 1/8 Sendok teh lada',
        proses:
            '- Rebus spaghetti hingga matang, tiriskan\n- Masukkan semua bahan ke dalam blender hingga menjadi pasta\n- Siapkan panci saus dan panaskan minyak zaitun\n- Tumis bawang bombay hingga harum\n- Masukkan pasta yang sebelumnya sudah dibuat, tambahkan daun basil, cincangan tomat kering, dan juga spaghetti\n- Aduk hingga rata, dan sajikan di atas piring'),
  ]),
  Makanan(
      nama: 'Japanese Food',
      imgAsset: 'assets/img/Japanese.png',
      listResep: [
        Masakan(
            name: 'Chicken charsiu',
            imageUrl:
                'https://cdn-brilio-net.akamaized.net/news/2020/06/05/185915/1242827-1000xauto-resep-chinese-food.jpg',
            bahan:
                '- 500 gr daging ayam bagian paha tanpa tulang\n- 3 sdm bumbu chairsiuw\n- 1.5 sdt angkak (jerang air panas, haluskan) atau bisa menggunakan pewarna merah\n- 2 siung bawang putih haluskan\n- 1 sdm minyak wijen\n- 1 sdt bumbu ngohiong\n- 1/2 sdt jahe parut\n- 2-3 sdm madu\n- 1 sdm saus tiram (optional) \n- Garam, gula, penyedap, lada secukupnya',
            proses:
                '- Tusuk-tusuk daging dengan garpu. Marinated dengan semua bumbu, masukkan kulkas, tutup dengan clingwrap semalaman atau minimal 3 jam. \n- Panggang di oven suhu 190 derajat sekitar 40 menit. \n- Setelah matang, akan ada banyak juice yang keluar, tiriskan. \n- Tuntaskan dengan cara bakar di atas teflon sampai sedikit mengering dan berasap.\n- Selama panggang di teflon, bolak-balik sembari dioles dengan madu dan sisa minyakpanggangan.\n- Potong-potong dan nikmati dengan mi atau nasi.'),
        Masakan(
            name: 'Mapo Tofu',
            imageUrl:
                'https://cdn-brilio-net.akamaized.net/news/2020/06/05/185915/1242828-1000xauto-resep-chinese-food.jpg',
            bahan:
                '- Tahu halus / tahu susu 1 kotak\n- 250 gr daging cincang\n- 1 plastik enoki\n- 10 siung bawang putih cincang\n- 1/2 biji bawang bombay, potong besar\n- 2 biji cabai merah besar, potong besar\n- Daun bawang potong kecil untuk garnish\n- 2 cm jahe, geprek\n- 2 sdm soybean paste / tauco\n- 1 sdm saus doubanjiang / szechuan sauce\n- 2 sdt merica\n- 2 sdt gula\n- 2 sdm maizena, larutkan\n- 200ml air putih\n- Minyak untuk menumis secukupnya',
            proses:
                '- Tumis bawang putih, bawang bombay dan jahe sampai layu. \n- Masukkan daging giling tumis hingga matang. \n- Masukkan bumbu halus, merica, gula, tambahkan 200 ml air, tes rasa. \nMasukkan tahu, enoki, cabai merah, daun bawang. Tuangkan larutan maizena. Tutup sebentar hingga tahu masak. \n- Siap dihidangkan.'),
        Masakan(
            name: 'Sapo Tahu',
            imageUrl:
                'https://cdn-brilio-net.akamaized.net/news/2020/06/05/185915/1242831-resep-chinese-food.jpg',
            bahan:
                '- 100 gram dada ayam (potong-potong kecil)\n- 3 buah firm tofu potong-potong\n- 1/2 ikat sawi hijau\n- 1/2 paprika merah iris tipis\n- 1 cm jahe (cincang halus) \n- 3 siung bawang putih (cincang) \n- 1 batang daun bawang (iris-iris) \n- 1 buah bawang bombay sedang (iris-iris) \n- 3 sdm minyak goreng\n- 1 sdm saus tiram\n- 2 sdm kecap ikan\n- garam secukupnya\n- merica bubuk secukupnya\n- gula pasir secukupnya\n- 1 sdm full maizena (larutkan) \n- 300 ml air',
            proses:
                '- Tumis jahe, bawang putih, bawang bombay, dan paprika. \n- Masukkan ayam, aduk hingga agak matang.\n- Masukkan sawi, aduk hingga agak layu. \n- Tambahkan saus tiram, kecap ikan, dan merica. Aduk rata, lalu tambahkan air, aduk kembali. \n- Tuang maizena, aduk rata, dan masak sampai meletup-letup. \n- Masukkan tahu dan daun bawang, beri garam. Aduk rata dan siap disantap.'),
        Masakan(
            name: 'Dimsum Ayam',
            imageUrl:
                'https://cdn-brilio-net.akamaized.net/news/2020/06/05/185915/1242832-resep-chinese-food.jpg',
            bahan:
                ' - 7 potong ayam bagian paha atas dan fillet bagian dagingnya saja.\n- 1 sdt garam\n- 1 sdm gula pasir\n- 1 sdm minyak wijen\n- 1/2 sdt lada halus\n- 3/4 sdm kecap asin\n- 3/4 sdm saus tiram\n- 25 gram sagu tani\n- 1 lembar daun bawang, iris.\n- 4 lembar besar kulit lumpia/pangsit/siomay. Bagi masing-masing menjadi 4 bagian.\n- 1 sdm parutan wortel untuk topping. \n- 5 sachet saus sambal\n- 1 sdt gula pasir\n- 3 sdm air panas, campur semua bahan saus jadi satu.',
            proses:
                '- Ambil 1/3 ayam fillet, cincang halus.\n- Blender 2/3 sisa ayam fillet sebentar saja (sekitar 3-5 detik) dan jangan terlalu halus. Campur jadi 1, lalu sisihkan. \n- Tambahkan garam, gula pasir, minyak wijen, lada halus, kecap asin, saus tiram, sagu tani, daun bawang, dan campur sampai rata. \n- Ambil 1 lembar kulit, isi sekitar 1 sdm adonan, lalu bentuk adonan dimsum. Lakukan sampai adonan habis. \n- Gunting ujung kulit yang bersisa agar adonan lebih rapi. Beri topping parutan wortel. \n- Kukus adonan selama kurang lebih 20 menit. Angkat dan sajikan dengan saus sambal sesuai selera.')
      ]),
  Makanan(nama: 'Chinese Food', imgAsset: 'assets/img/Chinese.png', listResep: [
    Masakan(
        name: 'Onigiri Tuna Mayo',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2020/06/22/186901/1253331-1000xauto-masakan-internasional-jepang.jpg',
        bahan:
            '- 300-400 gram beras\n- nori secukupnya\n- wijen hitam secukupnya - 1 cup tuna suwir\n- 2-3 sdm mayonaise\n- 1/4 bawang bombay, cincang dan tumis sebentar\n- 2 sdm kacang polong\n- 1 tangkai daun bawang dirajang\n- lada dan garam secukupnya',
        proses:
            '- Masak beras seperti biasa. \n- Campur semua bahan isi onigiri jadi satu. Sisihkan. \n- Ambil secukupnya nasi, beri isian tuna mayo, kepal-kepalkan hingga berbentuk segitiga. \n- Bungkus onigiri dengan nori, taburi wijen hitam.'),
    Masakan(
        name: 'Takoyaki',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2020/06/22/186901/1253337-masakan-internasional-jepang.jpg',
        bahan:
            '- 100 gram tepung terigu\n- 340 ml air/susu cair\n- 1 sendok teh kaldu bubuk\n- 1 sendok teh kecap ikan\n- 1 sendok teh bawang putih halus\n - 1 butir telur\n- 1 daun bawang\n- 2 buah gurita, iris kecil-kecil',
        proses:
            '- Blender semua bahan, kecuali gurita.\n- Tuangkan adonan ke cetakan kue cubit yang sudah dipanaskan. Masukkan irisan.\n- Balik adonan agar berbentuk bulat. Masak hingga kecokelatan.'),
    Masakan(
        name: 'Ramen',
        imageUrl:
            'https://cdn-brilio-net.akamaized.net/news/2020/06/22/186901/1253341-1000xauto-masakan-internasional-jepang.jpg',
        bahan:
            '- mie Telur\n- 2 buah Wortel iris korek api\n- daun bawang (iris) \n- rumput laut\n- suwiran daging sapi\n- bakso ikan\n- telur rebus\n- pokcoy\n- 2 siung bawang putih\n- 1/2 buah bawang bombay\n- 2 sdm kecap asin\n- 1 sdm saus tiram- 1 sdm Kecap ikan\n- merica\n- garam- ',
        proses:
            'Untuk kuah: Tumis bawang putih dan bombay. Tuang air rebusan daging sapi. Beri bumbu pelengkap, angkat kalau sudah mendidih. \n- Rebus mie dan pokcoy. \n- Taruh mie dan pokcoy di mangkok. Siram kuah, beri topping daging suwir, wortel iris, rumput laut, telur rebus, dan baso ikan.')
  ])
];
