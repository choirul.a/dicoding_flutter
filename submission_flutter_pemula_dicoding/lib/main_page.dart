import 'package:flutter/material.dart';
import 'package:submission_flutter_pemula_dicoding/credit.dart';
import 'package:submission_flutter_pemula_dicoding/list_resep.dart';
import 'model/makanan.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffebf8f1),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              alignment: Alignment.bottomCenter,
              height: 100,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/img/background.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RichText(
                      text: TextSpan(
                        text: 'Welcome\n',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 30),
                        children: [
                          TextSpan(
                            text: 'Let\'s Cook !',
                            style: TextStyle(
                                fontWeight: FontWeight.normal, fontSize: 20),
                          ),
                        ],
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.help_outline_rounded),
                      color: Colors.black54,
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return Credit();
                        }));
                      },
                    )
                  ],
                ),
              ),
            ),
            // SizedBox(height: 8),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: GridView.count(
                    crossAxisCount: 2,
                    crossAxisSpacing: 18,
                    mainAxisSpacing: 18,
                    childAspectRatio: 0.7,
                    children: makananList.map((data) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return ListResep(makanan: data);
                          }));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.withOpacity(0.3),
                                    spreadRadius: 2,
                                    blurRadius: 2)
                              ],
                              borderRadius: BorderRadius.circular(13),
                              color: Colors.amber[200]),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 25),
                                child: Text(
                                  data.nama,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 22,
                                      color: Colors.black54),
                                ),
                              ),
                              SizedBox(height: 30),
                              Expanded(
                                child: Image.asset(data.imgAsset),
                              )
                            ],
                          ),
                        ),
                      );
                    }).toList()),
              ),
            )
          ],
        ),
      ),
    );
  }
}
