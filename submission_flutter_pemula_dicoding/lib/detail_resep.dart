import 'package:flutter/material.dart';

import 'model/makanan.dart';

class DetailResep extends StatelessWidget {
  const DetailResep({Key? key, required this.detailResep}) : super(key: key);
  final Masakan detailResep;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(children: [
            Stack(children: [
              Container(
                height: 250,
                width: double.infinity,
                child: Image.network(
                  detailResep.imageUrl,
                  fit: BoxFit.fill,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.black45,
                  child: IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              )
            ]),
            SizedBox(height: 5),
            Container(
              padding: EdgeInsets.all(8),
              child: Column(
                children: [
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(
                            detailResep.name,
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'OpenSans'),
                          ),
                        ),
                        Favorite(detail: detailResep)
                      ]),
                  Divider(),
                  Text(
                    'Bahan',
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: 'OpenSans',
                    ),
                  ),
                  Text(
                    detailResep.bahan,
                    textAlign: TextAlign.center,
                  ),
                  Divider(),
                  Text(
                    'Cara Memasak',
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: 'OpenSans',
                    ),
                  ),
                  Text(
                    detailResep.proses,
                    textAlign: TextAlign.center,
                  )
                ],
              ),
            )
          ]),
        ),
      ),
    );
  }
}

class Favorite extends StatefulWidget {
  const Favorite({Key? key, required this.detail}) : super(key: key);
  final Masakan detail;

  @override
  _FavoriteState createState() => _FavoriteState();
}

class _FavoriteState extends State<Favorite> {
  bool isFavorite = false;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(isFavorite ? Icons.favorite : Icons.favorite_border_outlined),
      color: Colors.red,
      onPressed: () {
        setState(() {
          isFavorite = !isFavorite;

          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(isFavorite
                  ? 'Kamu Menyukai Resep ${widget.detail.name}'
                  : 'Kamu Batal Menyukai Resep ${widget.detail.name}'),
              duration: Duration(seconds: 2),
            ),
          );
        });
      },
    );
  }
}
