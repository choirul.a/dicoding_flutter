import 'package:flutter/material.dart';

class Credit extends StatelessWidget {
  const Credit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About'),
        backgroundColor: Colors.amber,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              Text(
                'Tentang Aplikasi',
                textAlign: TextAlign.start,
                style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Dosis'),
              ),
              Divider(),
              Text(
                'APlikasi ini dibuat sebagai syarat kelulusan kelas \"Kelas Membuat Aplikasi Flutter Untuk pemula" program ID Camp Indosat 2021',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, fontFamily: 'OpenSans'),
              ),
              SizedBox(height: 20),
              Text(
                'Credit',
                style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Dosis'),
              ),
              Divider(),
              Text(
                'Resep:\n https://www.briliofood.net\nhttps://www.idntimes.com/food\nhttps://cookpad.com\n\nAssets:\nhttps://www.freepik.com/\nhttps://www.behance.net/\nhttps://fonts.google.com/',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, fontFamily: 'OpenSans'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
