import 'package:flutter/material.dart';
import 'package:submission_flutter_pemula_dicoding/detail_resep.dart';
import 'package:submission_flutter_pemula_dicoding/model/makanan.dart';

class ListResep extends StatelessWidget {
  const ListResep({Key? key, required this.makanan}) : super(key: key);
  final Makanan makanan;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff4f3f1),
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Color(0xffebf8f1),
        elevation: 0,
        flexibleSpace: Image(
          image: AssetImage('assets/img/kitchen.jpg'),
          fit: BoxFit.fill,
        ),
        title: Text(
          makanan.nama,
          style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 20,
              fontFamily: 'Dosis'),
          textAlign: TextAlign.center,
        ),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Divider(),
            Expanded(
              child: ListView.builder(
                itemBuilder: (context, index) {
                  final Masakan resep = makanan.listResep[index];
                  // log(resep.imageUrl.toString());
                  return InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return DetailResep(
                          detailResep: resep,
                        );
                      }));
                    },
                    child: Card(
                      margin: EdgeInsets.all(5),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      child: Row(children: [
                        Expanded(
                            flex: 1,
                            child: ClipRRect(
                              clipBehavior: Clip.antiAlias,
                              borderRadius: BorderRadius.circular(8),
                              child: FadeInImage.assetNetwork(
                                placeholder: 'assets/img/placeholder.jpg',
                                image: resep.imageUrl,
                                fit: BoxFit.fill,
                              ),
                            )),
                        Expanded(
                          flex: 2,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              resep.name,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  fontFamily: 'Dosis'),
                            ),
                          ),
                        )
                      ]),
                    ),
                  );
                },
                itemCount: makanan.listResep.length,
              ),
            )
          ],
        ),
      ),
    );
  }
}
