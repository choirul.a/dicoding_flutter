import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/codelab1/detail_screen.dart';
import 'package:flutter_application_1/codelab1/main_screen.dart';
import 'package:flutter_application_1/codelab1/second_screen.dart';

void main() {
  runApp(MyApp());
}

class Heading extends StatelessWidget {
  final String text;

  const Heading({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Text(
        text,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontFamily: 'DancingScript',
            fontSize: 45.0),
      ),
    ));
  }
}

//=============== Statefull widget ======================

class BiggerText extends StatefulWidget {
  final String text;

  const BiggerText({Key? key, required this.text}) : super(key: key);

  @override
  _BiggerTextState createState() => _BiggerTextState();
}

class _BiggerTextState extends State<BiggerText> {
  double _textSize = 20.0;
  bool isPressed = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          widget.text,
          style: TextStyle(fontSize: _textSize),
        ),
        ElevatedButton(
          onPressed: () {
            setState(() {
              isPressed ? _textSize = 30 : _textSize = 10;

              isPressed = !isPressed;
            });
          },
          child: isPressed ? Text('Perbesar Text') : Text('Perkecil Text'),
        ),
        Text(isPressed.toString())
      ],
    );
  }
}

//============= Statelss Widget =======================

class FirstScreen extends StatelessWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text('First Screen', style: TextStyle(color: Colors.white)),
        actions: [
          IconButton(
              onPressed: (null),
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ))
        ],
        leading: IconButton(
            onPressed: (null),
            icon: Icon(
              Icons.menu,
              color: Colors.white,
            )),
      ),
      body: Center(
        child: Text('Hello Word !!'),
      ),
    );
  }
}

//=================padding=================

class LatihanPAdding extends StatelessWidget {
  const LatihanPAdding({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Container(
            color: Colors.indigo,
            width: double.infinity,
          ),
        ),
        Expanded(
          child: Container(
            width: double.infinity,
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    color: Colors.amber,
                    height: double.infinity,
                  ),
                ),
                Expanded(
                  child: Container(
                    height: double.infinity,
                    color: Colors.lightGreen,
                  ),
                ),
                Expanded(
                    child: Container(
                  height: double.infinity,
                  color: Colors.blue,
                ))
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            color: Colors.lightBlue,
            width: double.infinity,
          ),
        )
      ],
    );
  }
}

//=============DropDown Button=====================

class DropDOwnButton extends StatefulWidget {
  const DropDOwnButton({Key? key}) : super(key: key);

  @override
  _DropDOwnButtonState createState() => _DropDOwnButtonState();
}

class _DropDOwnButtonState extends State<DropDOwnButton> {
  String? language;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DropDOwn Button'),
      ),
      body: Center(
        child: DropdownButton(
          items: <DropdownMenuItem<String>>[
            DropdownMenuItem(
              child: Text('Dart'),
              value: 'Dart',
            ),
            DropdownMenuItem(
              child: Text('Kotlin'),
              value: 'Kotlin',
            ),
            DropdownMenuItem(
              child: Text("Swift"),
              value: 'Swift',
            ),
          ],
          value: language,
          hint: Text('Select Language'),
          onChanged: (String? value) {
            setState(() {
              language = value;
            });
          },
        ),
      ),
    );
  }
}

//========= Text Field =====================
class InputText extends StatefulWidget {
  const InputText({Key? key}) : super(key: key);

  @override
  _InputTextState createState() => _InputTextState();
}

class _InputTextState extends State<InputText> {
  String? _name;
  bool lightOn = false;
  String? language;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Input Widget'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            TextField(
              decoration: InputDecoration(
                  hintText: 'Write your name !!', labelText: 'Your name'),
              onChanged: (String value) {
                setState(() {
                  _name = value;
                });
              },
            ),
            SizedBox(
              height: 10.0,
            ),
            ElevatedButton(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(content: Text('Helloww $_name'));
                    });
              },
              child: Text('Submit'),
            ),
            SizedBox(height: 50.0),
            Switch(
                value: lightOn,
                onChanged: (bool value) {
                  setState(() {
                    lightOn = value;
                  });

                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(lightOn ? 'Light On' : 'Light Off'),
                      duration: Duration(seconds: 1),
                    ),
                  );
                }),
            SizedBox(height: 50.0),
            Column(
              children: [
                ListTile(
                  leading: Radio<String>(
                    value: 'Dart',
                    groupValue: language,
                    onChanged: (String? value) {
                      setState(
                        () {
                          language = value;
                          showSncakbar();
                        },
                      );
                    },
                  ),
                  title: Text('Dart'),
                ),
                ListTile(
                  leading: Radio<String>(
                    groupValue: language,
                    value: 'Kotlin',
                    onChanged: (value) {
                      setState(() {
                        language = value;
                        showSncakbar();
                      });
                    },
                  ),
                  title: Text('Kotlin'),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  void showSncakbar() {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text('$language selected'),
        duration: Duration(seconds: 1),
      ),
    );
  }
}

//========== ListView =======================
class LatihanListview extends StatelessWidget {
  const LatihanListview({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<int> listNumber = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    return Scaffold(
      body: ListView(
        children: listNumber.map((number) {
          return Container(
            height: 200,
            decoration: BoxDecoration(
              color: Colors.grey,
              border: Border.all(color: Colors.black),
            ),
            child: Center(
              child: Text(
                '$number',
                style: TextStyle(fontSize: 50),
              ),
            ),
          );
        }).toList(),
      ),
    );
  }
}

//========================= Latihan ListBuilder ======================
class LatihanListBuilder extends StatelessWidget {
  final List<int> listNumber = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  LatihanListBuilder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: 200,
            decoration: BoxDecoration(
              color: Colors.grey,
              border: Border.all(color: Colors.black),
            ),
            child: Center(
              child: Text(
                '${listNumber[index]}',
                style: TextStyle(
                  fontSize: 50,
                ),
              ),
            ),
          );
        },
        itemCount: listNumber.length,
      ),
    );
  }
}

//=============Latihan Listview.Separator/////////////////////////
class LatihanLisviewSeparator extends StatelessWidget {
  final List<int> listNumber = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  LatihanLisviewSeparator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.separated(
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: 200,
            decoration: BoxDecoration(
              color: Colors.grey,
              border: Border.all(color: Colors.black),
            ),
            child: Center(
              child: Text(
                '${listNumber[index]}',
                style: TextStyle(
                  fontSize: 50,
                ),
              ),
            ),
          );
        },
        separatorBuilder: (context, index) => Divider(),
        itemCount: listNumber.length,
      ),
    );
  }
}

//=================== Expanded ===========================
class Rainbow extends StatelessWidget {
  const Rainbow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Container(
            color: Colors.red,
          ),
        ),
        Expanded(
          child: Container(
            color: Colors.orange,
          ),
        ),
        Expanded(
          child: Container(
            color: Colors.yellow,
          ),
        ),
        Expanded(
          child: Container(color: Colors.green),
        ),
        Expanded(
          flex: 2,
          child: Container(
            color: Colors.blue,
          ),
        ),
        Expanded(
          child: Container(
            color: Colors.indigo,
          ),
        ),
        Expanded(
          child: Container(
            color: Colors.purple,
          ),
        ),
      ],
    );
  }
}

//================== Latihan Flexible ===================
class LatihanFlexible extends StatelessWidget {
  const LatihanFlexible({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Row(
              children: [ExpandedWidget(), FlexibleWidget()],
            ),
            Row(
              children: [ExpandedWidget(), ExpandedWidget()],
            ),
            Row(
              children: [FlexibleWidget(), FlexibleWidget()],
            ),
            Row(
              children: [FlexibleWidget(), ExpandedWidget()],
            )
          ],
        ),
      ),
    );
  }
}

class ExpandedWidget extends StatelessWidget {
  const ExpandedWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.teal,
          border: Border.all(color: Colors.white),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            'Expanded',
            style: TextStyle(
              fontSize: 24,
            ),
          ),
        ),
      ),
    );
  }
}

class FlexibleWidget extends StatelessWidget {
  const FlexibleWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.tealAccent,
          border: Border.all(color: Colors.white),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            'Flexib',
            style: TextStyle(
              fontSize: 24,
            ),
          ),
        ),
      ),
    );
  }
}

//=================== Navigation ==============

class FirstPage extends StatelessWidget {
  final String message = 'Hello From First Screen';

  const FirstPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Screen'),
      ),
      body: Center(
        child: ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => SecondScreen(
                    message: message,
                  ),
                ),
              );
            },
            child: Text('Pindah Screen')),
      ),
    );
  }
}

//=================== MediaQuery =============
class LatihanMediaQuery extends StatelessWidget {
  const LatihanMediaQuery({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    Orientation orientation = MediaQuery.of(context).orientation;

    return Scaffold(
      backgroundColor: Colors.blue,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Screen Width =${screenSize.width.toStringAsFixed(2)}',
              style: TextStyle(color: Colors.white, fontSize: 18),
              textAlign: TextAlign.center,
            ),
            Text(
              'Orientation = $orientation',
              style: TextStyle(fontSize: 18, color: Colors.white),
            )
          ],
        ),
      ),
    );
  }
}

//====================Latihan LayoutBuillder =================

class LatihanLayoutBuilder extends StatelessWidget {
  const LatihanLayoutBuilder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.blue,
      body: Row(
        children: [
          Expanded(
            child: LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'MediaQuery =${screenSize.width.toStringAsFixed(2)}',
                      style: TextStyle(color: Colors.white, fontSize: 18),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      'LayoutBuilder = ${constraints.maxWidth}',
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    )
                  ],
                );
              },
            ),
          ),
          Expanded(
            flex: 3,
            child: LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
                return Container(
                  color: Colors.white,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'MediaQuery =${screenSize.width.toStringAsFixed(2)}',
                          style: TextStyle(color: Colors.black, fontSize: 18),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          'LayoutBuilder = ${constraints.maxWidth}',
                          style: TextStyle(fontSize: 18, color: Colors.black),
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

//===================== Responsive layout ============

class ResponsivePage extends StatelessWidget {
  const ResponsivePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Responsive Layout'),
        ),
        body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            if (constraints.maxWidth < 600) {
              return ListView(
                children: _generateCOntainers(),
              );
            } else if (constraints.maxWidth < 900) {
              return GridView.count(
                crossAxisCount: 2,
                children: _generateCOntainers(),
              );
            } else {
              return GridView.count(
                  crossAxisCount: 6, children: _generateCOntainers());
            }
          },
        ));
  }
}

List<Widget> _generateCOntainers() {
  return List<Widget>.generate(20, (index) {
    return Container(
      margin: EdgeInsets.all(8),
      color: Colors.blue,
      height: 200,
    );
  });
}

//==============Main=============================
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.amber,
      ),
      home: MainScreen(),
    );
  }
}
